import React, { useEffect, useState } from 'react';
import Sidebar from './Sidebar';
import Chat from './Chat';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import API from './services/api';
import { setChats, setUsers, setWs } from './store/actions/chat.actions';
import { useDispatch, useSelector } from 'react-redux'
import config from './config/config';

function Home() {

    const dispatch = useDispatch();
    const { loggedInUserObj, signalProtocolManagerUser } = useSelector((state) => state.signal)
    const { chats } = useSelector((state) => state.chat)


    useEffect(() => {
        if (!(Object.keys(loggedInUserObj).length === 0)) {
            OnInit();
        }
    }, [loggedInUserObj]);

    const OnInit = async () => {
        try {
            console.log("Manager",signalProtocolManagerUser);
            console.log("loggedInUserObj",loggedInUserObj);
            let contacts = await API.getContacts(loggedInUserObj._id);
            dispatch(setUsers(contacts.data));
            let lsChats = JSON.parse(localStorage.getItem(`{${loggedInUserObj._id}}_messages`));
            dispatch(setChats(lsChats));
            let ws = new WebSocket(`${config.WS_URL}chat/${loggedInUserObj._id}`);
            console.log("socket connection", ws);

            ws.onopen = () => {
                console.log("connect websocker chat");
                dispatch(setWs(ws));

            }
            ws.onmessage = async (e) => {
                // debugger
                let new_message = JSON.parse(e.data);
                let tmpChats = localStorage.getItem(`{${loggedInUserObj._id}}_messages`);
                if (tmpChats) {
                    tmpChats = JSON.parse(tmpChats);
                } else tmpChats = {}
                // let chats = JSON.parse(localStorage.getItem(`{${loggedInUserObj._id}}_messages`));

                if (new_message.senderid == loggedInUserObj._id) {
                    new_message.message = localStorage.getItem('LastSentMessage');
                } else {
                    let decrypt_message = await signalProtocolManagerUser.decryptMessageAsync(new_message.senderid, new_message.message);
                    new_message.message = decrypt_message;
                }
                if (new_message.chatId in tmpChats) {
                    
                    const ochats = {
                        ...tmpChats,
                        [new_message.chatId]: {
                            ...tmpChats[new_message.chatId],
                            messages: [...tmpChats[new_message.chatId].messages.concat(new_message)]
                        }
                    }
                    dispatch(setChats(ochats));
                    localStorage.setItem(`{${loggedInUserObj._id}}_messages`, JSON.stringify(ochats));
                } else {
                    const newChat = {
                        chatId: new_message.chatId,
                        members: [new_message.senderid, new_message.receiverid],
                        messages: []
                    }
                    newChat.messages.push(new_message)
                    const newOchat = {
                        ...tmpChats,
                        [new_message.chatId]: newChat
                    }
                    dispatch(setChats(newOchat));
                    localStorage.setItem(`{${loggedInUserObj._id}}_messages`, JSON.stringify(newOchat));
                }

            };

            ws.onclose = () => {
                console.log("Disconnected Websocket");
            }


        } catch (err) {
            console.log(err);
        }
    }

    return (
        <div className="app_body">
            <Router>
                <Sidebar />
                <Switch>
                    <Route path="/rooms/:roomId">
                        <Chat />
                    </Route>
                    <Route path="/">
                    </Route>
                </Switch>
            </Router>
        </div>
    )
}

export default Home;