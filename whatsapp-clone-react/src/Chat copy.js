import React, { useState, useEffect, useRef } from 'react';
import { Avatar, IconButton } from '@material-ui/core';
import { AttachFile, MoreVert, SearchOutlined } from '@material-ui/icons';
import MicIcon from '@material-ui/icons/Mic';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import './Chat.css';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import API from './services/api';
import { setMessageToUser, setLastSentMessage } from './store/actions/chat.actions';

function Chat() {

    const dispatch = useDispatch();
    const [input, setInput] = useState("");
    const { roomId } = useParams();
    const [messages, setMessages] = useState([]);
    const endOfMessageRef = useRef(null);
    const { messageToUser, chats, ws } = useSelector((state) => state.chat)
    const { loggedInUserObj, signalProtocolManagerUser } = useSelector((state) => state.signal)




    useEffect(() => {
        if (roomId  && chats) {
            let lsChats = JSON.parse(localStorage.getItem(`{${loggedInUserObj._id}}_messages`));
            let selectedChatId = lsChats[selectedChatByUserChatId(roomId)];
            if (selectedChatId) setMessages(selectedChatId.messages);
        }
    }, [chats]);

    // useEffect(() => {
    //     console.log("getWs", ws)
    // }, [ws]);


    useEffect(() => {
        if (roomId) {
            selectedUser();
            let lsChats = localStorage.getItem(`{${loggedInUserObj._id}}_messages`) ? JSON.parse(localStorage.getItem(`{${loggedInUserObj._id}}_messages`)) : {};
            let selectedChatId = lsChats[selectedChatByUserChatId(roomId)];
            if (selectedChatId) setMessages(selectedChatId.messages);
            else setMessages([]);
        }
    }, [roomId]);

    const selectedUser = async () => {
        try {
            const Ouser = await API.getUserById(roomId);
            dispatch(setMessageToUser(Ouser.data));
        } catch (error) {
            console.log(error);
        }

    }

    const selectedChatByUserChatId= (roomId)=> {
        let selectedUserChatId = undefined;
        if (chats) {
            for (let chat of Object.values(chats)) {
                if (chat.members.includes(roomId)) {
                    selectedUserChatId = chat.chatId;
                    break;
                }
            }
        }
        return selectedUserChatId;
    }

    const getSelectedUserChatId = () => {
        let selectedUserChatId = undefined;
        if (chats) {
            for (let chat of Object.values(chats)) {
                if (chat.members.includes(messageToUser._id)) {
                    selectedUserChatId = chat.chatId;
                    break;
                }
            }
        }
        return selectedUserChatId;
    }

    const scrollToBottom = () => {
        endOfMessageRef.current.scrollIntoView({
            behavior: "smooth",
            block: "start",
        });
    };


    const sendMessage = async (e) => {
        e.preventDefault();
        // dispatch(setLastSentMessage(input));
        localStorage.setItem('LastSentMessage', input);
        let chatId = getSelectedUserChatId();
        let message = {
            message: input,
            timestamp: new Date(),
        }
        let messageBody = {
            chatId: chatId,
            senderid: loggedInUserObj._id,
            receiverid: messageToUser._id,
            ...message
        }
        try {

            let ecryptmessage = await signalProtocolManagerUser.encryptMessageAsync(messageToUser._id, message.message);
            messageBody.message = ecryptmessage;
            ws.send(JSON.stringify(messageBody));

        } catch (error) {
            console.log(error);
        }

        setInput("");

    }

    return (
        <div className='chat'>
            <div className='chat_header'>
                <Avatar src={messageToUser?.photoURL} />
                <div className='chat_headerInfo'>
                    <h3 className='chat-room-name'>{messageToUser?.displayName}</h3>
                    {/* <p className='chat-room-last-seen'>
                        Last seen {" "}
                        {new Date(
                            messages[messages.length - 1]?.
                                timestamp?.toDate()
                        ).toUTCString()}
                    </p> */}
                </div>
                <div className="chat_headerRight">
                    <IconButton>
                        <SearchOutlined />
                    </IconButton>
                    <IconButton>
                        <AttachFile />
                    </IconButton>
                    <IconButton>
                        <MoreVert />
                    </IconButton>

                </div>
            </div>
            <div className='chat_body'>
                {messages.map(message => (
                    <p className={`chat_message ${message.receiverid == loggedInUserObj._id && 'chat_receiver'}`}
                        key={message.messageId}>
                        {/* <span className="chat_name">{message.name}</span> */}
                        {message.message}
                        <span className="chat_timestemp">{new Date(message.timestamp).toUTCString()}</span>
                    </p>
                ))}

                {/* <div style={{ marginBottom: '50px' }} ref={endOfMessageRef} >
                </div> */}
            </div>
            <div className='chat_footer'>
                <InsertEmoticonIcon />
                <form>
                    <input value={input} onChange={(e) => setInput(e.target.value)} type="text" placeholder="Type a message" />
                    <button type="submit" onClick={sendMessage}> Send a Message</button>
                </form>
                <MicIcon />
            </div>

        </div>
    )
}

export default Chat
