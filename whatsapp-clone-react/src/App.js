import './App.css';
import React, { useEffect } from 'react';
import Login from './Login';
import Loading from './Loading';
import Home from './Home';
import { useSelector } from 'react-redux'
// import { useHistory } from "react-router-dom";

function App() {

  // const [user, loading] = useAuthState(auth);
  const { loggedInUserObj, isLoggedIn } = useSelector((state) => state.signal)
  // const history = useHistory();


  // useEffect(() => {
  //    if(!loggedInUserObj){
  //       history.push("/");
  //    }
  // }, [isLoggedIn])

  const getLayout = () => {
    // if (loading) return (<Loading />)
    if (!isLoggedIn) return (<Login />)
    return (<Home />)
  }

  return (
    <div className="app">
      {getLayout()}
    </div>
  );

}

export default App;
