import axios from 'axios'
import config from '../config/config'

const logIn = async (body) => {
    return new Promise(async (resolve, reject) => {
        try {
            let response = await axios.post(`${config.API_URL}api/login`, body)
            resolve(response)
        } catch (error) {
            reject(error)
        }
    })
}

const getContacts = async (_id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let response = await axios.get(`${config.API_URL}api/users/${_id}`)
            resolve(response)
        } catch (error) {
            reject(error)
        }
    })
}

const getUserById = async (_id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let response = await axios.get(`${config.API_URL}api/user/${_id}`)
            resolve(response)
        } catch (error) {
            reject(error)
        }
    })
}


const API = {
    logIn,
    getContacts,
    getUserById
}

export default API;