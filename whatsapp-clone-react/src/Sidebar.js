import React, { useState, useEffect } from 'react';
import './Sidebar.css';
import { Avatar, IconButton } from "@material-ui/core";
import DonutLargeIcon from "@material-ui/icons/DonutLarge";
import ChatIcon from "@material-ui/icons/Chat";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { SearchOutlined } from "@material-ui/icons";
import SidebarChat from "./SidebarChat";
import { auth } from './firebase';
import { useAuthState } from "react-firebase-hooks/auth";
import { signOut } from "firebase/auth";
import { setLoggedOutUser } from './store/actions/signal.action';
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from "react-router-dom";

function Sidebar(props) {

    const dispatch = useDispatch();
    const [rooms, setRooms] = useState([]);
    const loggedInUserObj = useSelector((state) => state.signal.loggedInUserObj)
    // const [user] = useAuthState(auth);
    const users = useSelector((state) => state.chat.users)
    const history = useHistory();
    

    // useEffect(() => {
    //     setRooms(users);
    // }, [users]);


    const LogOut = (auth) => {
        signOut(auth).then(() => {
            dispatch(setLoggedOutUser());
            history.push("/");
        }).catch((error) => {
            console.log(error);
        });
    }

    return (
        <div className="sidebar">
            <div className="sidebar_header">
                <Avatar src={loggedInUserObj.photoURL} alt="user_photo" onClick={() => LogOut(auth)} />
                <div className="sidebar_headerRight">
                    <IconButton>
                        <DonutLargeIcon />
                    </IconButton>
                    <IconButton>
                        <ChatIcon />
                    </IconButton>
                    <IconButton>
                        <MoreVertIcon />
                    </IconButton>

                </div>
            </div>
            <div className="sidebar_search">
                <div className="sidebar_searchContainer">
                    <SearchOutlined />
                    <input type="text" placeholder="Search or start new chat" />
                </div>
            </div>
            <div className="sidebar_chats">
                {/* <SidebarChat addNewChat /> */}
                {users?.map(room => (
                    <SidebarChat key={room._id} id={room._id} name={room.email} user={room} />
                ))}
            </div>
        </div>
    );
}

export default Sidebar;