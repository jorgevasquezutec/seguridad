const config = Object.freeze({
  API_URL: 'http://localhost:5000/',
  WS_URL: 'ws://localhost:5000/'
})
export default config
