import { Button } from '@material-ui/core';
import React from 'react';
import './Login.css';
import { auth, provider } from './firebase';
import { signInWithPopup } from 'firebase/auth';
import { useDispatch } from 'react-redux'
import { createSignalProtocolManager } from './signal/SignalGateway'
import { setLoggedInUser } from './store/actions/signal.action';
import { SignalServerStore } from "./signal/SignalGateway"
import API from './services/api'

function Login() {

    const dispatch = useDispatch();
    
    const signIn = async () => {
        try {
            const dummySingalService = new SignalServerStore();
            const result = await signInWithPopup(auth, provider)
            const user = await API.logIn(result.user);
            const manager = await createSignalProtocolManager(user.data.data._id, result.user.email, dummySingalService);
            dispatch(setLoggedInUser({
                loggedInUserObj: { ...result.user, _id: user.data.data._id },
                signalProtocolManagerUser: manager
            }));

        } catch (e) {
            console.log(e.code, e.message)
        }
    }
    return (
        <div className="login">
            <div className="login_container">
                <img src="https://upload.wikimedia.org/wikipedia/commons/6/6b/WhatsApp.svg" alt="" />
                <div className="login_text">
                    <h1>Sign in to Whatsapp</h1>
                </div>
                <Button type="submit" onClick={signIn}>Sign in With Google</Button>
            </div>
        </div>
    );
}

export default Login
