import { getApp, getApps, initializeApp } from 'firebase/app';
import { getAuth, GoogleAuthProvider } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';


const firebaseConfig = {
  apiKey: "AIzaSyA9a8eMl9exrASp8R7xS9C5w4FvZ-4HOAk",
  authDomain: "whats-app-clone-62f93.firebaseapp.com",
  projectId: "whats-app-clone-62f93",
  storageBucket: "whats-app-clone-62f93.appspot.com",
  messagingSenderId: "442153462599",
  appId: "1:442153462599:web:650ae7b942a27d02effa62",
  measurementId: "G-R4RXHPCNVY"
};

const app = getApps().length < 1 ? initializeApp(firebaseConfig) : getApp();
const db = getFirestore(app);
const auth = getAuth(app);
const provider = new GoogleAuthProvider();
export { db, auth, provider };
