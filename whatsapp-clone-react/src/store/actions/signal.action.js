import { signalType } from "../types/signal";

export const setLoggedInUser = (payload) => {
    console.log(payload)
    return {
        type: signalType.SET_LOGGED_IN_USER,
        payload
    }
}

export const setSignalProtocolManagerUser = (payload) => {
    // console.log("setSignalProtocolManagerUser", payload)
    return {
        type: signalType.SET_SIGNAL_PROTOCOL_MANAGER_USER,
        payload
    }
}

export const setLoggedOutUser = (payload) => ({
    type: signalType.SET_LOGGED_OUT_USER,
    payload
})