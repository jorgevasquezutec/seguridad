import {chatType} from '../types/chat';

export const setUsers = (payload) => ({
    type: chatType.SET_USERS,
    payload
})

export const setChats = (payload) => ({
    type: chatType.SET_CHATS,
    payload
})

export const setWs = (payload) => {
    // console.log("setWs", payload)
    return {
        type: chatType.SET_WS,
        payload
    }
}

export const setMessageToUser = (payload) => ({
    type: chatType.SET_MESSAGE_TO_USER,
    payload
})

export const setMsgText = (payload) => ({
    type: chatType.SET_MSG_TEST,
    payload
})

export const setLastSentMessage = (payload) => ({
    type: chatType.SET_LAST_SENT_MESSAGE,
    payload
})