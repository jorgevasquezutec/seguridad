export const chatType = {
    SET_USERS: 'SET_USERS',
    SET_MESSAGE_TO_USER: 'SET_MESSAGE_TO_USER',
    SET_WS: 'SET_WS',
    SET_CHATS: 'SET_CHATS',
    SET_LAST_SENT_MESSAGE: 'SET_LAST_SENT_MESSAGE',
    SET_MSG_TEST : 'SET_MSG_TEST'
}