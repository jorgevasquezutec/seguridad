import { combineReducers } from 'redux'


import chat from './chat.reducer';
import signal from './signal.reducer';


const rootReducer = combineReducers({
    chat,
    signal,
});

const reducers = (state, action) => {
    if (action.type === "RESET_STORE") {
        state = undefined;
    }
    return rootReducer(state, action)
}

export default reducers;