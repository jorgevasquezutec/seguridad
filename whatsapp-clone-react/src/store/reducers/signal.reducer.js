import { signalType } from "../types/signal"



const initialState = {
    isLoggedIn: false,
    loggedInUserObj: {},
    dummySingalService: null ,
    signalProtocolManagerUser: undefined,
}


const signalTypeReducer = (state= initialState, action) => {
    switch (action.type) {
        case signalType.SET_LOGGED_IN_USER:
            return {
                ...state,
                isLoggedIn: true,
                signalProtocolManagerUser: action.payload.signalProtocolManagerUser,
                loggedInUserObj: action.payload.loggedInUserObj
            }
        case signalType.SET_LOGGED_OUT_USER:
            return {
                ...state,
                isLoggedIn: false,
                loggedInUserObj: {},
                signalProtocolManagerUser : undefined
            }
        case signalType.SET_SIGNAL_PROTOCOL_MANAGER_USER:
            return {
                ...state,
                signalProtocolManagerUser: action.payload
            }
        default:
            return state
    }

}

export default signalTypeReducer;
