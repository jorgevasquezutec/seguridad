
import { chatType } from "../types/chat"

const initialState = {
    users: [],
    messageToUser: {},
    ws: null,
    chats: {},
    lastSentMessage: undefined,
    msgText: ""
}


const chatReducer = (state = initialState, action) => {
    switch (action.type) {
        case chatType.SET_USERS:
            return {
                ...state,
                users: action.payload
            }
        case chatType.SET_MESSAGE_TO_USER:
            return {
                ...state,
                messageToUser: action.payload
            }
        case chatType.SET_WS:
            return {
                ...state,
                ws: action.payload
            }
        case chatType.SET_CHATS:
            return {
                ...state,
                chats: action.payload
            }
        case chatType.SET_LAST_SENT_MESSAGE:
            return {
                ...state,
                lastSentMessage: action.payload
            }
        case chatType.SET_MSG_TEST:
            return {
                ...state,
                msgText: action.payload
            }
        default:
            return state
    }

}


export default chatReducer