import React, { useEffect, useState } from 'react';
import { Avatar } from "@material-ui/core";
import './SidebarChat.css';
import { Link } from 'react-router-dom';


function SidebarChat({ id, name, addNewChat, user }) {
    const [messages, setMessages] = useState("");    

    return (
        <Link to={`/rooms/${id}`} key={id}>
            <div className="sidebarChat">
                <Avatar src={user.photoURL} />
                <div className="sidebarChat_info">
                    <h2>{name}</h2>
                    {/* <p>{messages[0]?.message}</p> */}
                </div>
            </div>
        </Link>
    )

}

export default SidebarChat
