require('dotenv').config();
const http = require('./app/app');
const port = process.env.SERVER_PORT || 5000;
const Websocket = require('./app/socket/webSocket');

http.listen(port, () => {
    console.log(`listening on port ${port}`);
});

http.on("listening", () => {
    Websocket.InitWebSocket(http);
})




