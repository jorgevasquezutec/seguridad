const User = require('../models/User');


const logIn = async (req, res) => {

    try {
        let user = await User.findOne({ email: req.body.email });
        if (!user)  user = await User.create(req.body);
        else {
            user.photoURL = req.body.photoURL;
            await user.save();
        }
        return res.send({ message: "User LogIn", data: user });

    } catch (error) {
        return res.send(500).send({
            message: "Error inesperado",
            errorMesage: error?.message
        })

    }

}

const getUsers = async (req, res) => {
    try {
        const user = await User.find({ _id: {
            $ne: req.params._id
        } });
        return res.send(user);
    } catch (error) {
        return res.send(500).send({
            message: "Error inesperado",
            errorMesage: error?.message
        })
    }
}

const getUserById = async (req, res) => {
    try {
        const user = await User.findById(req.params._id);
        return res.send(user);
    } catch (error) {
        return res.send(500).send({
            message: "Error inesperado",
            errorMesage: error?.message
        })
    }
}


module.exports = {
    logIn,
    getUsers,
    getUserById
}