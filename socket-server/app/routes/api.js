const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController');


router.post('/login', UserController.logIn);
router.get('/users/:_id', UserController.getUsers);
router.get('/user/:_id', UserController.getUserById);



module.exports = router;
