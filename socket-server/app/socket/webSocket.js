const { v4: uuidv4 } = require('uuid');
var WebSocketServer = require('websocket').server;
// var http = require('http');

const InitWebSocket = (server) => {

    console.log("entro")
    const connections = [];

    const wss = new WebSocketServer({
        httpServer: server,
    })

    wss.on("request", (request) => {

        console.log("Websocket request received.")
        let connection = request.accept(null, request.origin)
        // console.log(connection)
        connections.push(connection)
        let senderid = request.httpRequest.url.split("/")[2]
        connection.userID = senderid

        connection.on("open", () => {
            console.log("Server socket Connection opened.")

        })
        connection.on("close", () => {
            console.log("Server socket Connection closed.")
        })

        connection.on('message', function (message) {
            let msgData = JSON.parse(message.utf8Data)
            // Create a new ID for new chat
            if (msgData.chatId === undefined) {
                msgData.chatId = uuidv4()
            }
            msgData.messageId = uuidv4()
            // Send message to Recipient Connection and the sender as well.
            connections.map(conn => {
                if (conn.userID == msgData.receiverid || conn.userID == msgData.senderid) {
                    conn.send(JSON.stringify(msgData))
                }
            })
        });
    })
}

module.exports= {
    InitWebSocket,
}