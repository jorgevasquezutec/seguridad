const express = require('express');
const config = require('./config/config');
const routes = require('./routes');
const bodyParser = require('body-parser');
const mongoose = require('mongoose')
const cors = require('cors');
const app = express();
const uri = process.env.MONGO_URI;



mongoose.connect(
    uri,
    {
        useNewUrlParser: true
    }
).then(() => console.log('connected')).catch(err => console.log(err))


app.use(cors({ exposedHeaders: ['Content-Disposition'] }));
app.set('llave', config.llave);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(express.json());
app.use(express.urlencoded({ limit: '50mb', extended: false }))
app.use('/', routes);
const http = require('http').createServer(app);





module.exports = http